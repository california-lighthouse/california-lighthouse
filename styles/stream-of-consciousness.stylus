/*
 * ### Table of Contents ###
 *
 * Preamble
 * Fonts
 * Basic Elements
 * Special Styles For Dialog
 * Scenes and Digressions
 */


/* ============================
	Preamble
============================ */

/*
 * In case all this seems kind of weird to you...
 *
 * The idea here is that in a stream-of-consciousness piece, the typography
 * should help represent the remembered events and/or the state of the
 * character's mind. So emphasis may be rendered as text changing size or color,
 * rather than switching to the italics style. Dialog may use different font
 * families to show the different voices, instead of quotation marks. And so on.
 */


/* ============================
	Fonts
============================ */

@font-face {}

/* ============================
	Basic Elements
============================ */

em
	font-style: inherit
	font-size: 125%
	line-height: 115%

strong
	font-weight: inherit
	font-size: 125%
	line-height: 110%
	color: red



/* ============================
	Special Styles For Dialog
============================ */

// Quotes without a class default to third-person speech.
q
blockquote
	font-family: "quicksand", sans-serif

q:before
q:after
	content: none

blockquote
	margin: 1em 4em

// "third" as in third-person speech.
q.third
blockquote.third
	font-family: "quicksand", sans-serif

// Nested third-person quotes.
q q
q blockquote
blockquote q
blockquote blockquote
	font-style: italic

// "first" as in first-person speech.
q.first
blockquote.first
	font-family: "alice", serif
	font-style: italic

// nested first-person quotes.
q.first q.first
blockquote.first q.first
q.first blockquote.first
blockquote.first blockquote.first
	font-size: 110%

// Use <s> tags for when the character is in denial, tries to suppress a thought.
s {}
// For when the character's line of thought trails off.
.trailing
	font-size: 83%



/* ============================
	Scenes and Digressions
============================ */

// I also have this idea that alternating pairs of scenes, digressions, or other
// sorts of distracted thoughts could be represented by changing the whitespace
// around the column of text.

// Alternating scenes.
.scene-A {}
.scene-B {}

aside

// Simultaneous thoughts, when it's a phrase.
.simultaneous-phrase-A {}
.simultaneous-phrase-B {}

// Simultaneous thoughts, for longer stretches...
.simultaneous-thoughts-A {}
.simultaneous-thoughts-B {}

// Some stories may also feature a sort of meta-narrator, the person commenting
// on the recorded consciousness.
.meta {}
