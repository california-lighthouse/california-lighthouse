# The first confirmed sighting of a camelops in thousands of years

<!--
	TODO: top image: pixelated image of the "camelclopse". The level of detail isn't great, but it's undeniably a camel with one big, green eye.
-->

A one-eyed, hallucination-inducing species of camel was spotted in the
San Diegan chaparral.

The camelops, a genus of camels indigenous to North America and once
thought long extinct, was seen and photographed yesterday in the Torrey
Pines State Natural Reserve. Although nowadays typically associated with
Eurasia and Africa, species of what would become the modern camel
actually originated in North America in the Eocene period, some 40 -- 50
million years ago, and then later migrated westward across the Bering
Strait. Until relatively recently, it was believed that the American
camels had gone extinct around the end of the Pleistocene period, some
10,000 years ago.

Beginning in the 1930s, rumors occasionally surfaced of strange camels
living in the arid and semiarid regions of California and Baja
California, with rare reports coming out of western Arizona. Such
rumors were usually dismissed out of hand, as the reports generally came
from people suffering extreme dehydration and wild, paradisiacal
hallucinations. In the 1970s, local scientists became interested in
these stories that never seemed to quite die off, but what little
evidence they gathered over the following decades remained inconclusive.
Until yesterday.

Trevor Weiss was hiking alone in the Torrey Pines Reserve. He decided to
leave the designated trail to try and climb an interesting-looking cliff
face. The cliff was sandy and unstable, and after climbing half the height,
he looked down and noticed some large animal walking directly beneath him.
Startled, he fell. Weiss lay very still for a moment, stunned, the wind
knocked out of him. When he fell, a large nearby animal had made a sound
like a foghorn and gracelessly ran a short distance away from him, then
paused to look back at him. Weiss groaned and sat up. And then he saw it.

The animal looked strange, and not just because it was some kind of
camel living in California. It had only a very small hump over its
shoulders, and it had only one, very large, green eye. The creature ---
what is already being called a monocular camelops, or "camelclopse" ---
watched him with its singular eye.

But as Weiss sat up, the camelops made an unsettling gurgling sound, then
sprayed him with a liquid that smelled like a strong cheese. Weiss thought
at first that it sneezed, but now agrees with scientists at the University
of California, San Diego that it *spat* at him. And then it ran off. Almost
instantly, Weiss's field of vision flooded with intense, rich, almost
painfully vivid colors. The sun shone beams of flowers and parrots, which
each shone floral sunbeams of their own; rainbow-colored women and cats
danced in the chromatic storm; and then the music rolled in, and Weiss was
lost to the world for the rest of the afternoon.

From there, the story will sound very familiar to anyone who has read or
heard about the Californian camel rumors: A red-faced, dehydrated, and
still-woozy feeling Weiss staggered as quickly as he could to Torrey
Pines Lodge to tell a skeptical audience about what he saw. What makes
Weiss's story different is that in the first moments of his psychedelic
vision, he thought to reach for his camera phone. At the time, Weiss had
intended to record the fantastical colors and shapes he was seeing. He
says "Everything seemed to be moving so quickly, and at the same time I
kind of lost all feeling of my body, couldn't tell where my hands were
going, could barely even *see* my own physical body, and I think in the
back of my mind I sorta worried that it was gonna be over before I could
figure out how to get my phone out of my pocket and the camera turned
on." He was still a little giddy when I had a chance to speak with him.

At the Torrey Pines Lodge, Elsa Cleland, an associate professor of
ecology at UCSD, happened to overhear Weiss's babbling story about the
camelops and she was the only one there who took him seriously. She grew
especially curious when he said he recorded the whole thing on his
phone, and was willing to wait with him for the battery to recharge.

<!--
	TODO: low-quality still from the video here.
-->

The actual video is very shaky, as Weiss's hands were trembling. And
then you can barely glimpse the camelops anyway, because Weiss soon
dropped his phone and forgot about trying to record anything until the
hallucinations had faded away.

Weiss and Cleland admit that the limited footage doesn't give much
information, even after Cleland analyzed a copy with colleagues back on
campus, but they and the rest of the Division of Biological Sciences are
buzzing with excitement anyway. More specific information is coming from
studies of Weiss's blood, as the camelops's "sneeze" apparently exposed
him to a whole new sort of hallucinogen, different from any yet
identified.

Professor emeritus Grayson Chao had specialized in genetics, and was
known on the UCSD campus for making facetious quips about how the
ecologists were studying a "soft, squishy, messy barely-science," and
yet even he has returned to join in the chatter. Chao says "It'd be sort
of like if someone not only came up with proof that Bigfoot is real, but
that the creature had actually been wandering around State parks
distributing LSD. How can you resist?"
