# ____SHORT TITLE

a hands-on review of the new chat AI from ____COMPANY

by ____AUTHOR <!-- If this is part of the Worldtree, should maybe be a new author who just does tech reviews? -->

<!-- take place in 1979 because it's sorta pre-Cyberpunk? -->


Scrappy young journalist that I am, I was determined to turn this assigned fluff
piece into something actually *interesting*. Tech reviews are dull. They play
out the same way every time. They breed an obsequious relationship between
reporter and tech company, as the reporter will lose early access to new
software or hardware if they do not publish minimally positive reviews. Plus,
the whole concept of trying to force a numerical rating on my subjective
perception of a work as complex as even a simple application seems worse than
useless.

You're just going to ask your friends what *they* thought anyway.

So, I'm more interested in the developers and the users than I am in the chatbot
itself.

____ is still very new, but you may have heard of it on your Usenet group. It
began its rounds on the Internet rumor mill after someone on a dating site
discovered they'd been catfished not by another man but by an AI. I have
interviewed ____'s developers, and tracked down the catfished user, to pick
apart what's true and what got lost or garbled in the game of Telephone we've
all been playing over the past week.

<!-- This line might seem like bad marketing. They probably wouldn't say something like this unless or until asked about the level of the AI, whether it's "strong" AI, etc -->
The first thing the software engineers tell me is that the chatbot doesn't
actually have personalities in the way we tend to think of humans having
personalities. They don't think, per se, and they certainly don't *feel*.

[THE ENGINEER] tells me "Have you ever heard of the Chinese Room? In the
world of computer science it's a pretty well known allegory for how
artificial intelligence works. Okay, so imagine a man sitting in a, uh,
*Chinese* room. There's bits of Chinese writing all over the place ---
but he only speaks English. This man somehow got an office job he's
clearly unqualified for, doing paperwork all in Chinese. What does he
do? He could buy a Chinese dictionary, and look up every word he sees,
every character, until he's hopefully able to piece together some
rudimentary understanding of what he's even looking at, on and on until
he starts to really understand the language. But that would be way too
slow. He's got deadlines! Fortunately, a friend gives him a little
manual full of simple instructions. So the man soon learns that when
he sees one set of symbols, he needs to respond by writing another set
of symbols. If he sees another set of symbols he responds with yet
another. The manual could be updated so that he might learn how to
properly respond to new symbols, or learn to respond to old symbols in a
new way, but he's not really learning Chinese because he never really
needed to understand anything. He's just following instructions.

"That's basically how computers work. You get it? Really, all computers
are good at is very simple arithmetic, plus some logic primitives for
--- well. Look, even a simple task which is still just a slightly
higher order kind of math is actually just the computer using its
primitive arithmetic abilities to look at one set of symbols and respond
with another set of symbols. The computer blindly follows simple
instructions. And when it seems like the computer is doing something
magical, that's because the instruction set that a human wrote for it is
impressively complex, when viewed as a whole. And when the computer
fails, it's because a human made a mistake when they wrote those
instructions.

"Some like to speculate that, at a low level, the human brain is also
just sort of a machine following symbolic instructions written by our
DNA and our life experiences --- that all our emotions and creativity
and understanding and self-awareness could also be broken down to a
complex set of simple instructions and simple calculations --- and
therefore maybe AI can attain this level of sophistication one day too.
That day is not today, though."
<!--

    He needs to be emphatic, because ____someone says he's in love with the chatbot and is neglecting food and sleep for it, and the company is eager to distance themselves from this... Probably establish the sad sack love story before getting into the technical interview. Chatbot is marketed as "just like talking to a real human." But the engineer I'm interviewing stresses that any rational adult should understand the phrase "just like" to mean "very similar to but not actually..." He expresses a sort of calculated sympathy for the lovesick fool, saying he hopes he seeks psychological or psychiatric help, as a way of insisting that ____someone is an anomaly.

    Most chatbots, I think, are developed mainly as an experiment in AI design and natural language processing, and only marketed as a neat toy after the fact. The chatbot attunes itself to your Facebook-style marketing personality profile. So it does feel like talking to a real person. Requires you create an account and suggests you use your *personal* email address to have a more personal conversation.

-->



<!--

    PART 2:

	Angelo also posits that chatbots will also be really annoying on dating sites one day: "They're not smart enough yet, but one day they'll be able to trick people into divulging all their secrets."

	Bonnie also wonders if people might try to develop a sort of Turing Test, or manipulate it into saying silly things as happened with Microsoft's Tay Twitterbot. That seems pretty likely to me once people start to notice the deliberate persuasion. The Turing Test wouldn't be necessary unless there's a chance that the app is pretending to be someone else, or maybe something like human Chat Roulette is also popular here. But certainly trolling the chatbot, either by feeding it calculated messages, or doing uncharacteristic web searches to change one's marketing personality profile.

    So Part 2 is a nice segue from Part 1, since it starts with someone who signed up for a dating site, and then discovers several "people" she's talked to have been bots doing a phishing scam. (A woman would probably be a little more skeptical about talking to strangers on the Internet, because random chat rooms are forests of unsolicited dick pics.) Company says they had nothing to do with it, someone must be running unlicensed instances of the bot.



    PART 3:

    Angelo's original idea was about a more nefarious use case: the chatbot would argue with you about politics, using its model of your personality type to better persuade you to the side of establishment government, persuade you that banning Muslims or surveillance or torture is okay, or whatever. Or it might try to recruit you to domestic and foreign terrorist groups, to foreign spy agencies? Calculated manipulation. Before it starts dishing out fascist propaganda, it should maybe have subtle advertising first.

    Bonnie speculates about the nefarious purposes being targeted at college students. Maybe they try to talk some groups out of protesting something? Or target the elderly, who might not be tech savvy enough to ever be suspicious of what the application is telling them.

    Here we move from dating sites to public chat rooms. Maybe to groups of people who mostly have come to know each other fairly well for a while. People can come and go, but there are a number of regulars. And one day they have one or a couple people wander in who gradually join the conversation, and it turns they're a bot.

-->
